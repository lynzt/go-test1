FROM golang:1.11-alpine3.8
WORKDIR /go/src/app
COPY . .
CMD [ "go", "run", "./main.go" ]

# docker run -it -v {$PWD}:/go/src/app --rm go/news